﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleController : MonoBehaviour
{
    [SerializeField] GameObject onGameObject = default;
    [SerializeField] GameObject offGameObject = default;

    Toggle toggle;

    private void Start()
    {
        toggle = GetComponent<Toggle>();
        Toggle(toggle.isOn);
    }
    public void OnChange(bool value)
    {
        Toggle(value);
    }

    private void Toggle(bool value)
    {
        onGameObject.SetActive(value);
        offGameObject.SetActive(!value);
        Image image;
        if (value)
            image = onGameObject.GetComponentInChildren<Image>();
        else
            image = offGameObject.GetComponentInChildren<Image>();
        toggle.targetGraphic = image;
    }
}
