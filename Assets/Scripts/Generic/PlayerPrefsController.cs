﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsController : MonoBehaviour
{
    const string MASTER_VOLUME_KEY = "master-volume";
    const string DIFFICULTY_KEY = "difficulty";

    const float MIN_VOLUME = 0f;
    const float MAX_VOLUME = 1f;

    public static void SetMasterVolume(float volume)
    {
        float vol = Mathf.Max(MIN_VOLUME, Mathf.Min(volume, MAX_VOLUME));
        PlayerPrefs.SetFloat(MASTER_VOLUME_KEY, vol);
    }

    public static float GetMasterVolume()
    {
        return PlayerPrefs.GetFloat(MASTER_VOLUME_KEY);
    }

    public static void SetDifficulty(bool difficulty)
    {
        int dif = difficulty ? 1 : 0;
        PlayerPrefs.SetInt(DIFFICULTY_KEY, dif);
    }

    public static bool GetDifficulty()
    {
        var difficulty = PlayerPrefs.GetInt(DIFFICULTY_KEY);
        return difficulty == 1 ? true : false;
    }
}
