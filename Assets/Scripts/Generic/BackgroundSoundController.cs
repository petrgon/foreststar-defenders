﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSoundController : MonoBehaviour
{
    public static BackgroundSoundController Instance { get; private set; } = null;

    [SerializeField] AudioClip intro = default;
    [SerializeField] AudioClip background = default;

    AudioSource audioSource;

    void Awake()
    {
        if (Instance != null && Instance != this) {
            Destroy(gameObject);
            return;
        } else {
            Instance = this;
        }
        DontDestroyOnLoad(gameObject);
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsController.GetMasterVolume();
    }

    private void Update()
    {
        if (!audioSource.isPlaying)
        {
            if(audioSource.clip == intro)
            {
                audioSource.clip = background;
                audioSource.loop = true;
            }
            else
            {
                audioSource.PlayDelayed(15f);
            }
        }
    }

    public void SetVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
