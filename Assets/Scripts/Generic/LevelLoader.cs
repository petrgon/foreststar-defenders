﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{

    public static LevelLoader Instance { get; private set; } = null;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void LoadlSplashScreen()
    {

        SceneManager.LoadScene("Splash Scene");
    }
    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main Menu Scene");
    }

    public void LoadOptions()
    {
        SceneManager.LoadScene("Options Scene");
    }

    public void LoadFirstLevel()
    {
        SceneManager.LoadScene("Level 1");
    }

    public void LoadLevel(int level)
    {
        SceneManager.LoadScene("Level " + level);
    }

    public void LoadNextScene()
    {
        int loadedScene = SceneManager.GetActiveScene().buildIndex;
        if (loadedScene == SceneManager.sceneCount - 1) // last scene goto menu
            LoadMainMenu();
        SceneManager.LoadScene(loadedScene + 1);
    }

    public void RestartScene()
    {
        int loadedScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(loadedScene);
    }

    public void LoadExitGame()
    {
        Application.Quit();
    }

}
