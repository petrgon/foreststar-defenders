﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "SpawningGroup", menuName = "ScriptableObjects/SpawningGroup", order = 2)]
[System.Serializable]
public class SpawningGroup : ScriptableObject
{
    public float startAt = 30;
    public float endAt = 600;
    public int count = 0;
    public float everySec = 0;
    public Attacker[] attackers = default;
    
    private int alreadySpawned = 0;
    private float duration => Mathf.Max(endAt - startAt, 1);

    public int HowMuchSpawnNow(float time)
    {
        int shouldBeAlreadySpawned = 0;
        if (count != 0 && count <= alreadySpawned)
            shouldBeAlreadySpawned = Mathf.FloorToInt(
                    Mathf.Min(duration, time - startAt) * (count / duration)
                );
        else if (everySec != 0 && time < endAt)
            shouldBeAlreadySpawned = Mathf.FloorToInt(
                    (time - startAt) / everySec
                    );
        int spawnNow = Mathf.Max(shouldBeAlreadySpawned - alreadySpawned, 0);
        alreadySpawned = Mathf.Max(shouldBeAlreadySpawned, 0);
        return spawnNow;
    }

}