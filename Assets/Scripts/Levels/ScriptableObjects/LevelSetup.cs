﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSetup", menuName = "ScriptableObjects/LevelSetup", order = 1)]
public class LevelSetup : ScriptableObject
{
    public int startingMoney = 50;
    [Tooltip("Level length in SECONDS, make it one bigger than End At time of the last Spawning Group.")]
    public float roundDuration = 600;
    public Vector2[] starsLocation = default;
    public SpawningGroup[] spawningGroups = default;


    public GameObject[] WhatToSpawnNow(float time)
    {
        throw new NotImplementedException();
    }
}
