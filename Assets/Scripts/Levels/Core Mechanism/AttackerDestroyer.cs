﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerDestroyer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Attacker>() != null)
        {
            Destroy(collision.gameObject);
        }
    }
}
