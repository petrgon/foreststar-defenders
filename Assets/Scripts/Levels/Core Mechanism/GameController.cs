﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public float LevelDuration { get; set; }
    public SpawningGroup[] SpawningGroups { get; set; }

    [SerializeField] Slider gameProgress = default;

    Star[] stars = default;
    AttackerSpawner[] attackerSpawners = default;

    int capturedStars = 0;
    InGameMenuController inGameMenuController;
    int aliveAttackersCount = 0;

    private void Start()
    {
        stars = FindObjectsOfType<Star>();
        attackerSpawners = FindObjectsOfType<AttackerSpawner>();
        inGameMenuController = FindObjectOfType<InGameMenuController>();
        InvokeRepeating("SpawnEnemies", 0f, 0.3f);
    }

    private void Update()
    {
        if (capturedStars == stars.Length)
            inGameMenuController.GameOver();
        if(Time.timeSinceLevelLoad >= LevelDuration && aliveAttackersCount <= 0)
            inGameMenuController.Winner(stars.Length - capturedStars);
        gameProgress.value = Time.timeSinceLevelLoad / LevelDuration;
    }

    private void SpawnEnemies()
    {
        float t = Time.timeSinceLevelLoad;
        foreach(SpawningGroup group in SpawningGroups)
        {
            int count = group.HowMuchSpawnNow(t);
            for(int i = 0; i < count; i++)
                SpawnEnemy(group.attackers[Random.Range(0, group.attackers.Length)]);
        }
    }

    private void SpawnEnemy(Attacker attacker)
    {
        attackerSpawners[Random.Range(0, attackerSpawners.Length)].SpawnEnemy(attacker);
    }

    public void StarCaptured()
    {
        capturedStars++;
    }

    public void AttackerSpawned()
    {
        aliveAttackersCount++;
    }

    public void AttackerDied()
    {
        aliveAttackersCount--;
    }
}
