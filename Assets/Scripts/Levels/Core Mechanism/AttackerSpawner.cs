﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour
{
    const string ATTACKER_PARENT_NAME = "Attackers";

    [SerializeField] float delaySpawn = 1f;

    GameObject attackersParrent;

    private void Start()
    {
        CreateAttackersParrent();
    }


    public void SpawnEnemy(Attacker attacker)
    {
        StartCoroutine(SpawnDelayed(Random.Range(0, delaySpawn),attacker));
    }

    private IEnumerator SpawnDelayed(float time, Attacker attacker)
    {
        yield return new WaitForSeconds(time);
        Attacker spawned = Instantiate(attacker, transform.position, transform.rotation);
        spawned.transform.parent = attackersParrent.transform;
    }
    private void CreateAttackersParrent()
    {
        attackersParrent = GameObject.Find(ATTACKER_PARENT_NAME);
        if (!attackersParrent)
            attackersParrent = new GameObject(ATTACKER_PARENT_NAME);
    }
}
