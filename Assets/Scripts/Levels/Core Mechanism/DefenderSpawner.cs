﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour
{
    const string DEFENDER_PARENT_NAME = "Defenders";

    Buymenu buymenu;
    MoneyDisplay moneyDisplay;
    GameObject defenderParent;

    private void Start()
    {
        buymenu = FindObjectOfType<Buymenu>();
        moneyDisplay = FindObjectOfType<MoneyDisplay>();
        CreateAttackersParrent();
    }

    private void OnMouseDown()
    {
        SpawnDefender(GetMouseSquare());
    }

    private void SpawnDefender(Vector2 pos)
    {
        if (!IsOccupied(pos))
            if (buymenu.EnoughMoneyForSelected())
            {
                var selected = buymenu.GetSelectedDefender();
                var newDefender = Instantiate(selected, pos, selected.transform.rotation);
                newDefender.transform.parent = defenderParent.transform;
                buymenu.PayForSelected();
                if (Input.GetAxis("Queue") != 1)
                    buymenu.Deselect();
            }
    }

    private Vector2 GetMouseSquare()
    {
        Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        pos = Camera.main.ScreenToWorldPoint(pos);
        pos = new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
        return pos;
    }

    private bool IsOccupied(Vector2 pos)
    {
        var defenders = FindObjectsOfType<Defender>();
        foreach (Defender def in defenders)
            if (def.GetComponent<Transform>().position == (Vector3)pos)
                return true;
        return false;
    }

    private void CreateAttackersParrent()
    {
        defenderParent = GameObject.Find(DEFENDER_PARENT_NAME);
        if (!defenderParent)
            defenderParent = new GameObject(DEFENDER_PARENT_NAME);
    }

}
