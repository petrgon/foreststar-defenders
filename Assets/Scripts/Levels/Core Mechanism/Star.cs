﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    [SerializeField] Sprite active = default;
    [SerializeField] Sprite inactive = default;

    SpriteRenderer spriteRenderer;
    Animator animator;
    Vulnerable vulnerable;
    bool captured = false;

    private void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        spriteRenderer.sprite = active;
        animator = GetComponent<Animator>();
        vulnerable = GetComponent<Vulnerable>();
        StartCoroutine(IdleCoroutine(Random.Range(0, 0.5f)));
        StartCoroutine(AnimationCoroutine());
    }

    private void Update()
    {
        if (!captured && vulnerable.Health <= 0)
            CaptureStar();
    }

    private IEnumerator AnimationCoroutine()
    {
        while (!captured)
        {
            yield return new WaitForSeconds(Random.Range(45, 120));
            animator.SetTrigger("jump");
        }
    }

    private void CaptureStar()
    {
        captured = true;
        spriteRenderer.sprite = inactive;
        FindObjectOfType<GameController>().StarCaptured();
    }

    private IEnumerator IdleCoroutine(float time)
    {
        yield return new WaitForSeconds(time);
        animator.SetTrigger("scale");
    }
}
