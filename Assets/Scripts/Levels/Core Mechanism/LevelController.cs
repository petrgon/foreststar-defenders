﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public static LevelController Instance { get; private set; } = null;

    [SerializeField] Star star = default;
    [SerializeField] int actualLevel = 0;

    [SerializeField] LevelSetup[] levelsEasy = default;
    [SerializeField] LevelSetup[] levelsHard = default;

    private GameController gameController;
    private LevelLoader levelLoader;

    LevelSetup[] levels = default;

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
            Instance.Initialize();
            return;
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(gameObject);
        Initialize();
    }

    private void Initialize()
    {
        gameController = FindObjectOfType<GameController>();
        levelLoader = FindObjectOfType<LevelLoader>();
        var difficulty = PlayerPrefsController.GetDifficulty();
        if (difficulty)
            levels = levelsHard;
        else
            levels = levelsEasy;
        LoadLevel(actualLevel);
    }

    private void LoadLevel(int levelNumber)
    {
        if(levelNumber >= levels.Length)
        {
            levelLoader.LoadMainMenu();
            Destroy(gameObject);
            return;
        }
        LevelSetup level = levels[levelNumber];
        gameController.LevelDuration = level.roundDuration;
        gameController.SpawningGroups = level.spawningGroups;
        FindObjectOfType<Buymenu>().MoneyTotal = level.startingMoney;
        SpawnStars(level.starsLocation);
    }

    private Star[] SpawnStars(Vector2[] starsCoords)
    {
        Star[] stars = new Star[starsCoords.Length];
        for (int i = 0; i < starsCoords.Length; i++)
        {
            stars[i] = Instantiate(star, starsCoords[i], Quaternion.identity);
            stars[i].transform.parent = gameController.transform;
        }
        return stars;
    }

    public void RestartLevel()
    {
        levelLoader.RestartScene();
    }

    public void LoadNextLevel()
    {
        actualLevel++;
        levelLoader.LoadNextScene();
    }
}
