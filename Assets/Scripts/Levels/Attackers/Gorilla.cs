﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gorilla : MonoBehaviour
{
    Animator animator;
    bool isJumping = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isJumping)
        {
            Defender otherDefender = collision.GetComponent<Defender>();
            if (otherDefender != null)
            {
                if (collision.GetComponent<Melee>() != null)
                {
                    animator.SetTrigger("jumpTrigger");
                    isJumping = true;
                    return;
                }
                Vulnerable otherVulnerable = collision.GetComponent<Vulnerable>();
                if (otherVulnerable != null)
                {
                    gameObject.GetComponent<Attacker>().Attack(otherVulnerable);
                    return;
                }
            }
        }

    }

    public void JumpEnded()
    {
        isJumping = false;
    }

}
