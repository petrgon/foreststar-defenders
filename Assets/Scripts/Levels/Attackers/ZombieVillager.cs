﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieVillager : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Defender otherDefender = collision.GetComponent<Defender>();
        if (otherDefender != null)
        {
            Vulnerable otherVulnerable = collision.GetComponent<Vulnerable>();
            if (otherVulnerable != null)
                gameObject.GetComponent<Attacker>().Attack(otherVulnerable);
        }
    }
}
