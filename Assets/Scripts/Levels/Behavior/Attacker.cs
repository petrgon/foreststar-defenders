﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [Range(0f, 2f)] [SerializeField] float currentMovementSpeed = 0.5f;
    [SerializeField] int damage = 15;

    float movementSpeedOrigin;
    Vulnerable currentTarget;
    Rigidbody2D rb;
    GameController gameController;

    void Awake()
    {
        movementSpeedOrigin = currentMovementSpeed;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        gameController = FindObjectOfType<GameController>();
        gameController.AttackerSpawned();
    }

    private void OnDestroy() => gameController.AttackerDied();

    private void Update() => UpdateAnimationState();

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + currentMovementSpeed * Vector3.left * Time.deltaTime);
    }

    private void UpdateAnimationState()
    {
        if (!currentTarget)
        {
            GetComponent<Animator>().SetBool("isAttacking", false);
            return;
        }
        else if (currentTarget.Health <= 0)
        {
            currentTarget = default;
            GetComponent<Animator>().SetBool("isAttacking", false);
            return;
        }
    }

    public void ProcessAttack()
    {
        if (!currentTarget) return;
        currentTarget.DealDamage(damage);
    }

    public void Attack(Vulnerable target)
    {
        GetComponent<Animator>().SetBool("isAttacking", true);
        currentTarget = target;
    }

    public void StartMoving() => currentMovementSpeed = movementSpeedOrigin;

    public void StopMoving() => currentMovementSpeed = 0;
}
