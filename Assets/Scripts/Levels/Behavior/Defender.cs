﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour
{
    public Sprite GetSprite()
    {
       return GetComponentInChildren<SpriteRenderer>().sprite;
    }
}
