﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float movementSpeed = 2f;
    [SerializeField] int damage = 15;

    bool used = false;
    object objectlock = new object();

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * movementSpeed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Vulnerable vulnerable = collision.GetComponentInParent<Vulnerable>();
        Attacker attacker = collision.GetComponentInParent<Attacker>();
        if (vulnerable && attacker)
        {
            lock (objectlock)
            {
                if (!used)
                {
                    used = true;
                    vulnerable.DealDamage(damage, transform.position);
                    Destroy(gameObject);
                }
            }
        }
    }
}
