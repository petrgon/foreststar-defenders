﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vulnerable : MonoBehaviour
{

    [SerializeField] int health = 100;
    [SerializeField] ParticleSystem hitted = default;

    Animator animator;

    public int Health { get => health; }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public bool DealDamage(int damage)
    {
        health -= damage;
        return ResolveDying();
    }

    public bool DealDamage(int damage, Vector2 contact)
    {
        InstantiateParticles(contact);
        return DealDamage(damage);
    }

    private bool ResolveDying()
    {
        if (health <= 0)
        {
            GetComponent<Collider2D>().enabled = false;
            animator.SetBool("isDying", true);
            return true;
        }
        return false;
    }

    private void InstantiateParticles(Vector2 contact)
    {
        if (hitted != default)
        {
            ParticleSystem particle = Instantiate(hitted, contact, hitted.transform.rotation);
            particle.transform.parent = transform;
            Destroy(particle.gameObject, 2f);
        }
    }

    public void Destroy() => Destroy(gameObject);
}
