﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastSpell : MonoBehaviour
{
    [SerializeField] SpellGeneric spell = default;
    
    public void castSpell()
    {
        spell.CastSpell(gameObject, gameObject);
    }
}
