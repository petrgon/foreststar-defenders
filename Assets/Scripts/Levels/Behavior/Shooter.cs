﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    const string PROJECTILES_PARENT_NAME = "Projectiles";

    [SerializeField] GameObject bow = default;

    [SerializeField] GameObject projectile = default;

    AttackerSpawner myLaneSpawner;

    Animator animator;

    GameObject projectilesParent;

    private void Start()
    {
        animator = GetComponent<Animator>();
        CreateProjectilesParrent();
    }

    private void Update()
    {
        if (IsAttackerInLane())
            animator.SetBool("isAttacking", true);
        else
            animator.SetBool("isAttacking", false);
    }


    private bool IsAttackerInLane()
    {
        var attackers = FindObjectsOfType<Attacker>();
        foreach (var attacker in attackers)
        {
            if (Mathf.Abs(attacker.transform.position.y - transform.position.y) <= Mathf.Epsilon)
            {
                return true;
            }
        }
        return false;
    }
    
    public void Fire()
    {
        var go = Instantiate(projectile, bow.transform.position, bow.transform.rotation);
        go.transform.parent = projectilesParent.transform;
    }
    private void CreateProjectilesParrent()
    {
        projectilesParent = GameObject.Find(PROJECTILES_PARENT_NAME);
        if (!projectilesParent)
            projectilesParent = new GameObject(PROJECTILES_PARENT_NAME);
    }
}
