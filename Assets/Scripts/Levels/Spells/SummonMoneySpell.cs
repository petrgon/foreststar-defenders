﻿using UnityEngine;
using System.Collections;

public class SummonMoneySpell : SpellGeneric
{
    [SerializeField] int moneyModifier = 3;
    [Header("Technicalies - do not change")]
    [SerializeField] GameObject particlesEffect = default;
    [SerializeField] float destroyAfterSeconds = 2;
    [SerializeField] float effectAfterSeconds = 1;

    private Buymenu Buymenu;

    private void Start()
    {
        Buymenu = FindObjectOfType<Buymenu>();
        StartCoroutine(DoEffect());
    }

    public override void CastSpell(GameObject caster, GameObject target)
    {
        GameObject spell = Instantiate(gameObject);
        spell.transform.parent = caster.transform;
        GameObject effect = Instantiate(particlesEffect, caster.GetComponent<Transform>());
        effect.transform.parent = spell.transform;
        Destroy(effect, destroyAfterSeconds);
        Destroy(spell, destroyAfterSeconds);
    }

    private IEnumerator DoEffect()
    {
        yield return new WaitForSeconds(effectAfterSeconds);
        Buymenu.AddMoney(moneyModifier);
    }
}
