﻿using UnityEngine;
using System.Collections;

public abstract class SpellGeneric : MonoBehaviour
{
    public abstract void CastSpell(GameObject caster, GameObject target);
}
