﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using TMPro;
using UnityEngine;

public class MoneyDisplay : MonoBehaviour
{
    int moneyTotal;
    TextMeshProUGUI text;

    public int MoneyTotal
    {
        set
        {
            moneyTotal = value;
            UpdateDisplay();
        }
    }

    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    private void UpdateDisplay()
    {
        text.text = moneyTotal.ToString();
    }
}
