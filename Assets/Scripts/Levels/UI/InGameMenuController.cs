﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InGameMenuController : MonoBehaviour
{
    [Header("Canvases")]
    [SerializeField] GameObject gameOverCanvas = default;
    [SerializeField] GameObject winnerCanvas = default;
    [SerializeField] GameObject pauseCanvas = default;

    bool gameIsPaused = false;
    bool gameEndMenuIsActive = false;

    private LevelLoader levelLoader;

    private void Start()
    {
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    private void Update()
    {
        if (!gameEndMenuIsActive && Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
                Resume();
            else if (!gameEndMenuIsActive)
                Pause();
        }
    }

    public void Pause()
    {
        pauseCanvas.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    public void GameOver()
    {
        gameOverCanvas.SetActive(true);
        gameEndMenuIsActive = true;
        gameOverCanvas.GetComponentInChildren<StarsMenuController>().ShowStars(0);
    }

    public void Winner(int acquiredStars)
    {
        Winner(acquiredStars, 5);
    }

    public void Winner(int acquiredStars, int starsMax)
    {
        int showAcquiredStars = acquiredStars * 5 / starsMax; // 5 is max showable stars
        winnerCanvas.SetActive(true);
        gameEndMenuIsActive = true;
        winnerCanvas.GetComponentInChildren<StarsMenuController>().ShowStars(showAcquiredStars);
    }

    public void Restart()
    {
        Time.timeScale = 1f;
        FindObjectOfType<LevelController>().RestartLevel();
    }

    public void NextLevel()
    {
        FindObjectOfType<LevelController>().LoadNextLevel();
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        Destroy(FindObjectOfType<LevelController>().gameObject);
        levelLoader.LoadMainMenu();
    }

    public void Resume()
    {
        pauseCanvas.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }
}
