﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BuymenuElement : MonoBehaviour
{
    static readonly Color GRAY = new Color32(94, 94, 94, 255);
    static readonly Color LIGHT_GRAY = new Color32(165, 165, 165, 255);
    static readonly Color WHITE = new Color32(255, 255, 255, 255);
    static readonly Color RED = new Color32(255, 57, 57, 255);

    [SerializeField] int price = 1;
    [SerializeField] Defender defender = default;

    [Header("References to Children")]
    [SerializeField] TextMeshProUGUI _priceMesh = default;
    [SerializeField] GameObject _defender = default;
    [SerializeField] GameObject _leather = default;
    [SerializeField] GameObject _plank = default;
    [SerializeField] GameObject _button = default;

    Buymenu buymenu;

    public int Price { get => price; set { price = value; _priceMesh.SetText(value.ToString()); } }

    public Defender Defender { get => defender; set => defender = value; }

    public bool Selected { get; private set; } = false;


    private void Awake()
    {
        if (Defender != default)
        {
            _priceMesh.SetText(price.ToString());
            _priceMesh.color = GRAY;
            _defender.GetComponent<Image>().sprite = Defender.GetComponent<Defender>().GetSprite();
            _defender.GetComponent<Image>().color = GRAY;
        }
        buymenu = FindObjectOfType<Buymenu>();
    }

    private void Start()
    {
        HighlightPrice();
    }

    public void Available(bool b)
    {
        _plank.SetActive(b);
        _leather.SetActive(b);
        _defender.SetActive(b);
        _button.SetActive(b);
    }

    public void OnClick()
    {
        var elements = FindObjectsOfType<BuymenuElement>();
        foreach (var element in elements)
            element.Select(false);
        Select(true);
    }

    public void Select(bool select)
    {
        Selected = select;
        if (select)
        {
            if (buymenu.MoneyTotal < Price)
            {
                HighlightUnavailable();
                Selected = false;
                return;
            }
            else
                _defender.GetComponent<Image>().color = WHITE;
        }
        else
        {
            HighlightPrice();
        }
        Selected = select;
    }

    public void HighlightPrice()
    {
        if (buymenu.MoneyTotal >= Price)
        {
            _priceMesh.color = WHITE;
            _defender.GetComponent<Image>().color = Selected ? WHITE : LIGHT_GRAY;
        }
        else
        {
            _priceMesh.color = GRAY;
            _defender.GetComponent<Image>().color = GRAY;
        }
    }

    public void HighlightUnavailable()
    {
        StartCoroutine(HighlightUnavailableCoroutine());
    }

    private IEnumerator HighlightUnavailableCoroutine()
    {
        Color orig = _priceMesh.color;
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.1f);
            _priceMesh.color = RED;
            yield return new WaitForSeconds(0.1f);
            _priceMesh.color = orig;
        }
    }

}
