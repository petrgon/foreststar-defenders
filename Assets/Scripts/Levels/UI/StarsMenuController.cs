﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarsMenuController : MonoBehaviour
{
    [SerializeField] StarInMenu[] starsInMenu = default;

    public void ShowStars(int acquiredStars)
    {
        for(int i = 0; i < starsInMenu.Length; i++)
        {
            if (i < acquiredStars)
                starsInMenu[i].Show(true);
            else
                starsInMenu[i].Show(false);
        }
    }
}
