﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarInMenu : MonoBehaviour
{
    [SerializeField] Sprite acquiredSprite = default;
    [SerializeField] Sprite unacquiredSprite = default;

    [SerializeField] bool acquired = false;

    Image image;
    Animator animator;

    public bool Accquired { get => acquired; set { acquired = value; Show(acquired); } }

    private void Awake()
    {
        image = GetComponentInChildren<Image>();
        animator = GetComponent<Animator>();
    }

    public void Show(bool acquired)
    {
        if (acquired)
        {
            image.sprite = acquiredSprite;
            animator.SetTrigger("acquired");
        }
        else
        {
            image.sprite = unacquiredSprite;
            animator.SetTrigger("unacquired");

        }
    }
}
