﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buymenu : MonoBehaviour
{
    [SerializeField] int activeElements = 8;
    [SerializeField] BuymenuElement[] elements = default;

    private MoneyDisplay moneyDisplay;

    public int MoneyTotal { get; set; } = default;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = activeElements; i < elements.Length; i++)
            elements[i].Available(false);
        moneyDisplay = FindObjectOfType<MoneyDisplay>();
        moneyDisplay.MoneyTotal = MoneyTotal;
        UpdatePricesHighlightion();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
            Deselect();
    }

    public Defender GetSelectedDefender()
    {
        BuymenuElement selected = GetSelected();
        if (selected != null)
            return selected.Defender;
        return null;
    }

    public bool PayForSelected()
    {
        if (!EnoughMoneyForSelected())
            return false;
        var selected = GetSelected();
        if (selected == null)
            return false;
        SubstractMoney(selected.Price);
        UpdatePricesHighlightion();
        return true;
    }

    public void Deselect()
    {
        foreach (var ele in elements)
            ele.Select(false);
    }

    public bool EnoughMoneyForSelected()
    {
        return GetSelected() == null ? false : GetSelected().Price <= MoneyTotal;
    }

    private BuymenuElement GetSelected()
    {
        foreach (var ele in elements)
            if (ele.Selected)
                return ele;
        return null;
    }

    private void UpdatePricesHighlightion()
    {
        foreach (BuymenuElement ele in elements)
            ele.HighlightPrice();
    }

    public void AddMoney(int value)
    {
        MoneyTotal += value;
        moneyDisplay.MoneyTotal = MoneyTotal;
        UpdatePricesHighlightion();
    }

    public void SubstractMoney(int value)
    {
        AddMoney(-value);
    }

}
