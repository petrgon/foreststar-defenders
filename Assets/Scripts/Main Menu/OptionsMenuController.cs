﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenuController : MonoBehaviour
{
    [SerializeField] Slider volumeSlider = default;
    [SerializeField] Toggle difficultyToggle = default;
    [SerializeField] float defaultVolume = 0.8f;
    [SerializeField] bool defaultDifficulty = false;
    private LevelLoader levelLoader;

    private void Start()
    {
        levelLoader = FindObjectOfType<LevelLoader>();
        volumeSlider.value = PlayerPrefsController.GetMasterVolume();
        difficultyToggle.isOn = PlayerPrefsController.GetDifficulty();
    }
    private void Update()
    {
        var backgroundMusic = FindObjectOfType<BackgroundSoundController>();
        if (backgroundMusic)
        {
            backgroundMusic.SetVolume(volumeSlider.value);
        }
        else
        {
            Debug.LogWarning("BackgroundSoundController not found, did you started from splash screen?");
        }
    }

    public void SaveClick()
    {
        PlayerPrefsController.SetMasterVolume(volumeSlider.value);
        PlayerPrefsController.SetDifficulty(difficultyToggle.isOn);
        levelLoader.LoadMainMenu();
    }

    public void CancelClick()
    {
        volumeSlider.value = PlayerPrefsController.GetMasterVolume();
        difficultyToggle.isOn = PlayerPrefsController.GetDifficulty();
        levelLoader.LoadMainMenu();
    }

    public void DefaultsClick()
    {
        volumeSlider.value = defaultVolume;
        difficultyToggle.isOn = defaultDifficulty;
    }
}
