﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    private LevelLoader levelLoader;

    private void Start()
    {
        levelLoader = FindObjectOfType<LevelLoader>();
    }

    public void OptionsClick()
    {
        levelLoader.LoadOptions();
    }

    public void StartGameClick()
    {
        levelLoader.LoadFirstLevel();
    }

    public void EndGameClick()
    {
        levelLoader.LoadExitGame();
    }
    
}
