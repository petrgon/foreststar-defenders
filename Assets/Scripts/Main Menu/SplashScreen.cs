﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
    [SerializeField] Slider slider = default;
    [SerializeField] float duration = 5f;


    // Update is called once per frame
    void Update()
    {
        float elapsedTime = Time.time / duration;
        slider.value = elapsedTime;
        if (elapsedTime >= 1)
            FindObjectOfType<LevelLoader>().LoadMainMenu();
    }
}
